package com.up.midivisualizer.midi;

/**
 *
 * @author GW-Ricky
 */
public class Note {
    
    long start;
    long duration;
    int pitch;
    int velocity;
    int channel;

    public Note(long start, long duration, int pitch, int velocity, int channel) {
        this.start = start;
        this.duration = duration;
        this.pitch = pitch;
        this.velocity = velocity;
        this.channel = channel;
    }

    public long getStart() {
        return start;
    }

    public long getDuration() {
        return duration;
    }

    public int getPitch() {
        return pitch;
    }

    public int getVelocity() {
        return velocity;
    }

    public int getChannel() {
        return channel;
    }
    
}
