package com.up.midivisualizer.midi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author GW-Ricky
 */
public class MidiChannel {
    
    private ArrayList<Note> notes = new ArrayList<>();
    private ArrayList<Note> played = new ArrayList<>();
    private ArrayList<Note> buffered = new ArrayList<>();
    int id;
    private static final int ignorelen = 10000;
    private int instrument = 0;

    public MidiChannel(int id) {
        this.id = id;
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }
    
    public void addNote(Note n) {
        notes.add(n);
    }
    
    public List<Note> getNewNotes(double tick) {
        List<Note> ns = notes.stream().filter(n -> n.getStart() < tick && n.getStart() - tick < ignorelen && !played.contains(n)).collect(Collectors.toList());
        played.addAll(ns);
        return ns;
    }
    
    public List<Note> getNewBufferedNotes(double tick, int buffer) {
        List<Note> ns = notes.stream().filter(n -> n.getStart() - buffer < tick && n.getStart() - buffer - tick < ignorelen && !buffered.contains(n)).collect(Collectors.toList());
        buffered.addAll(ns);
        return ns;
    }
    
    public void cleanupNotes(double tick) {
        notes.stream().filter(n -> n.getStart() + ignorelen < tick).forEach(buffered::remove);
        buffered.stream().filter(n -> n.getStart() + ignorelen < tick).forEach(buffered::remove);
    }
    
    public void reset() {
        played.clear();
        buffered.clear();
    }

    public int getId() {
        return id;
    }

    public void setInstrument(int instrument) {
        this.instrument = instrument;
    }

    public int getInstrument() {
        return instrument;
    }
    
}
