package com.up.midivisualizer.midi;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

/**
 *
 * @author GW-Ricky
 */
public class MidiParser {
    
    public static MidiSong parse(File f) throws InvalidMidiDataException, IOException {
        Sequence s = MidiSystem.getSequence(f);
        double res = -1;
        if (s.getDivisionType() == Sequence.PPQ) {
            res = s.getResolution();
        } else {
            throw new RuntimeException("Unsupported timing type");
        }
        MidiSong song = new MidiSong();
        song.setTempo(res / 500000 * 1000);
        for (Track t : s.getTracks()) {
            LinkedList<Entry<ShortMessage, Long>> queue = new LinkedList<>();
            for (int i = 0; i < t.size(); i++) {
                MidiEvent evt = t.get(i);
                MidiMessage m = evt.getMessage();
                if (m instanceof ShortMessage) {
                    ShortMessage sm = (ShortMessage)m;
                    if (sm.getCommand() == ShortMessage.NOTE_ON) {
                        for (int j = 0; j < queue.size(); j++) {
                            Entry<ShortMessage, Long> cursm = queue.get(j);
                            if (cursm.getKey().getChannel() == sm.getChannel() && cursm.getKey().getData1() == sm.getData1()) {
                                queue.remove(cursm);
                                song.getChannels()[sm.getChannel()].addNote(new Note(cursm.getValue(), evt.getTick() - cursm.getValue(), sm.getData1(), cursm.getKey().getData2(), sm.getChannel()));
                                break;
                            }
                        }
                        if (sm.getData2() > 0) {
                            queue.add(new Entry<>(sm, evt.getTick()));
                        }
                    }
                    if (sm.getCommand() == ShortMessage.NOTE_OFF) {
                        for (int j = 0; j < queue.size(); j++) {
                            Entry<ShortMessage, Long> cursm = queue.get(j);
                            if (cursm.getKey().getChannel() == sm.getChannel() && cursm.getKey().getData1() == sm.getData1()) {
                                queue.remove(cursm);
                                song.getChannels()[sm.getChannel()].addNote(new Note(cursm.getValue(), evt.getTick() - cursm.getValue(), sm.getData1(), cursm.getKey().getData2(), sm.getChannel()));
                                break;
                            }
                        }
                    }
                    if (sm.getCommand() == ShortMessage.PROGRAM_CHANGE) {
                        song.getChannels()[sm.getChannel()].setInstrument(sm.getData1());
                    }
                }
                if (m instanceof MetaMessage) {
                    MetaMessage mm = (MetaMessage)m;
                    if (mm.getType() == 0x51) {
                        int mpq = (mm.getData()[0] & 0xFF) << 16 | (mm.getData()[1] & 0xFF) << 8 | (mm.getData()[2] & 0xFF);
                        song.setTempo(res / mpq * 1000);
                    }
                }
            }
        }
        return song;
    }
    
    private static class Entry<K, V> {
        K key;
        V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
        
    }
    
}
