package com.up.midivisualizer.midi;

/**
 *
 * @author GW-Ricky
 */
public class MidiSong {
    private MidiChannel[] channels = new MidiChannel[16];
    private double tempo;

    public MidiSong() {
        for (int i = 0; i < 16; i++) {
            channels[i] = new MidiChannel(i);
        }
    }

    public double getTempo() {
        return tempo;
    }

    public void setTempo(double tempo) {
        this.tempo = tempo;
    }

    public MidiChannel[] getChannels() {
        return channels;
    }
    
}
