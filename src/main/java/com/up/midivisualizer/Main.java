package com.up.midivisualizer;

import com.jogamp.opengl.GL2;
import com.up.midivisualizer.instrument.BasicInstrument;
import com.up.midivisualizer.instrument.RainInstrument;
import com.up.midivisualizer.instrument.RenderedInstrument;
import com.up.midivisualizer.midi.MidiChannel;
import com.up.midivisualizer.midi.MidiParser;
import com.up.midivisualizer.midi.MidiSong;
import com.up.midivisualizer.midi.Note;
import com.up.pe.math.Point2D;
import com.up.pe.math.Point3D;
import com.up.pe.render.DisplayManager;
import com.up.pe.render.Light;
import com.up.pe.render.Material;
import java.awt.Color;
import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

/**
 *
 * @author GW-Ricky
 */
public class Main {
    
    static double tps = 0;
    
    
    static long do_buffer_len = 0;
    static long do_notes_len = 0;
    static long do_ticks_len = 0;
    static long do_pending_len = 0;
    
    public static void main(String[] args) throws MidiUnavailableException {
        try {
            File mf = new File("Enya - Only Time.mid");
            MidiSong song = MidiParser.parse(mf);
            Synthesizer synth = MidiSystem.getSynthesizer();
            synth.open();
            
            ArrayList<RenderedInstrument> ris = new ArrayList<>();
            for (int i = 0; i < 16; i++) ris.add(new RainInstrument(new Point3D(0, 0, -i)));
//            for (int i = 8; i < 16; i++) ris.add(new BasicInstrument(new Point3D(0, 0, -i)));
            
            Frame f = new Frame();
            f.setSize(1000, 800);
            DisplayManager dm = new DisplayManager() {
                @Override
                public void preRender(GL2 gl) {
//                    drawAxis(gl, 10);
                }

                @Override
                public void postRender(GL2 gl) {
                    renderString("FPS: " + getFPS(), Color.red, new Point2D(10, 10));
                    renderString("TPS: " + tps, Color.red, new Point2D(10, 20));
                    renderString("Buffer: " + do_buffer_len / 100, Color.red, new Point2D(10, 30));
                    renderString("Notes: " + do_notes_len / 100, Color.red, new Point2D(10, 40));
                    renderString("Ticks: " + do_ticks_len / 100, Color.red, new Point2D(10, 50));
                    renderString("Pending: " + do_pending_len / 100, Color.red, new Point2D(10, 60));
                }
            };
            f.add(dm.getCanvas());
            f.setVisible(true);
            
            dm.addDefaultFlyControls(1, 1);
            dm.addLight(new Light(new Point3D(13, 10, 3), Material.white, 100));
            dm.addLight(new Light(new Point3D(-3, 10, 3), Material.white, 100));
            
            dm.addMeshes(ris.stream().map(ri -> ri.getMesh()).collect(Collectors.toList()));
            
            Thread renderer = new Thread(() -> {
                while (true) {
                    dm.getCanvas().repaint();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) { }
                }
            });
            renderer.start();
            
            
            
            for (MidiChannel c : song.getChannels()) synth.getChannels()[c.getId()].programChange(c.getInstrument());
            
            Thread player = new Thread(() -> {
                double tick = 0;
                ArrayList<Note> pending = new ArrayList<>();
                while (true) {
                    long tstart = System.currentTimeMillis();
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException ex) { }
                    
                    for (MidiChannel c : song.getChannels()) {
                        RenderedInstrument ri = ris.get(c.getId());
                        long dblstart = System.nanoTime();
                        for (Note n : c.getNewBufferedNotes(tick, ri.getBufferSize())) {
                            ri.addBufferedNote(n);
                        }
                        do_buffer_len = System.nanoTime() - dblstart;
                        long dnlstart = System.nanoTime();
                        for (Note n : c.getNewNotes(tick)) {
                            ri.addNote(n);
                            synth.getChannels()[n.getChannel()].noteOn(n.getPitch(), n.getVelocity());
                            pending.add(n);
                        }
                        do_notes_len = System.nanoTime() - dnlstart;
                        long dtlstart = System.nanoTime();
                        ri.tick(tick);
                        do_ticks_len = System.nanoTime() - dtlstart;
                        c.cleanupNotes(tick);
                    }
                    
                    long dplstart = System.nanoTime();
                    for (int i = 0; i < pending.size(); i++) {
                        Note n = pending.get(i);
                        if (n.getStart() + n.getDuration() < tick) {
                            i--;
                            pending.remove(n);
                            synth.getChannels()[n.getChannel()].noteOff(n.getPitch(), 63);
                        }
                    }
                    do_pending_len = System.nanoTime() - dplstart;
                    
                    tps = 1000d / (System.currentTimeMillis() - tstart) * song.getTempo();
                    tick += (System.currentTimeMillis() - tstart) * song.getTempo();
                }
            });
            player.start();
            
        } catch (InvalidMidiDataException | IOException ex) {
            ex.printStackTrace();
        }
    }
}
