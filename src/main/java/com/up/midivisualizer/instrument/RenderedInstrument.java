package com.up.midivisualizer.instrument;

import com.up.midivisualizer.midi.Note;
import com.up.pe.mesh.Mesh;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author GW-Ricky
 */
public abstract class RenderedInstrument {
    private CopyOnWriteArrayList<Note> notes = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<Note> buffer = new CopyOnWriteArrayList<>();
    
    public void addBufferedNote(Note n) {
        buffer.add(n);
        bufferNote(n);
    }
    
    public void addNote(Note n) {
        notes.add(n);
        buffer.remove(n);
        startNote(n);
    }
    
    public void tick(double t) {
        for (Note n : notes) {
            if (n.getStart() + n.getDuration() <= t) {
                notes.remove(n);
                endNote(n);
            } else {
                updateNote(n, (int)(t - n.getStart()));
            }
        }
        for (Note n : buffer) {
            updateBuffer(n, getBufferSize() - (int)(n.getStart() - t));
        }
    }
    
    public abstract void bufferNote(Note n);
    public abstract void updateBuffer(Note n, int ticks);
    public abstract void startNote(Note n);
    public abstract void updateNote(Note n, int ticks);
    public abstract void endNote(Note n);
    public abstract Mesh getMesh();
    public abstract int getBufferSize();
    
}
