package com.up.midivisualizer.instrument;

import com.up.midivisualizer.midi.Note;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.GroupableSimpleMesh;
import com.up.pe.mesh.Mesh;
import com.up.pe.mesh.MeshGenerator;
import com.up.pe.mesh.MeshGroup;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.render.Material;
import com.up.pe.render.Vertex;
import java.awt.Color;

/**
 *
 * @author GW-Ricky
 */
public class BasicInstrument extends RenderedInstrument {
    
    private MeshGroup mesh;
    
    Material off = new Material(Color.gray, Color.white, Color.black, 0, null);
    Material on = new Material(Color.green, Color.white, Color.black, 0, null);
    
    public BasicInstrument(Point3D pos) {
        mesh = new MeshGroup(pos, Angle3D.zeros, new Vector3D(0.25, 0.25, 0.25));
        for (int i = 0; i < 127; i++) {
            mesh.addMesh(new GroupableSimpleMesh(MeshGenerator.cube(off, new Point3D(i / 2d, 0, 0), Angle3D.zeros, new Vector3D(0.25, 0.75, 0.75))));
        }
    }
    
    @Override
    public Mesh getMesh() {
        return mesh;
    }

    @Override
    public void startNote(Note n) {
        SimpleMesh sm = (SimpleMesh)mesh.getMeshes().get(n.getPitch()).getMesh();
        for (Vertex v : sm.getVerticies()) v.setMaterial(on);
        sm.setPosition(new Point3D(sm.getPosition().getX(), 1, 0));
    }

    @Override
    public void updateNote(Note n, int ticks) {
        SimpleMesh sm = (SimpleMesh)mesh.getMeshes().get(n.getPitch()).getMesh();
        sm.setPosition(new Point3D(sm.getPosition().getX(), 1 - (double)ticks / n.getDuration(), 0));
    }

    @Override
    public void endNote(Note n) {
        SimpleMesh sm = (SimpleMesh)mesh.getMeshes().get(n.getPitch()).getMesh();
        for (Vertex v : sm.getVerticies()) v.setMaterial(off);
        sm.setPosition(new Point3D(sm.getPosition().getX(), 0, 0));
    }

    @Override
    public void bufferNote(Note n) {
        
    }

    @Override
    public void updateBuffer(Note n, int ticks) {
        
    }

    @Override
    public int getBufferSize() {
        return 10;
    }
}
