package com.up.midivisualizer.instrument;

import com.up.midivisualizer.midi.Note;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.GroupableSimpleMesh;
import com.up.pe.mesh.Mesh;
import com.up.pe.mesh.MeshGenerator;
import com.up.pe.mesh.MeshGroup;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.render.Material;
import java.awt.Color;
import java.util.HashMap;

/**
 *
 * @author GW-Ricky
 */
public class RainInstrument extends RenderedInstrument {
    
    private MeshGroup parent;
    private HashMap<Note, GroupableSimpleMesh> drops = new HashMap<>();
    private HashMap<Note, GroupableSimpleMesh> splashes = new HashMap<>();
    private static final double cheight = 25;
    
    public RainInstrument(Point3D pos) {
        parent = new MeshGroup(pos, Angle3D.zeros, new Vector3D(0.25, 0.25, 0.25));
        GroupableSimpleMesh landing = new GroupableSimpleMesh(MeshGenerator.cube(new Material(new Color(32, 32, 128), Color.white, Color.black, 0, null), new Point3D(0, 0, 0), Angle3D.zeros, new Vector3D(64, 0.01, 4)));
        parent.addMesh(landing);
        GroupableSimpleMesh cloud = new GroupableSimpleMesh(MeshGenerator.cube(new Material(new Color(128, 128, 128), Color.white, Color.black, 0, null), new Point3D(0, cheight, 0), Angle3D.zeros, new Vector3D(64, 1, 2)));
        parent.addMesh(cloud);
    }
    
    private void createDrop(Note n) {
        GroupableSimpleMesh sm = new GroupableSimpleMesh(MeshGenerator.cube(new Material(new Color(100, 100, 150 + (int)(Math.random() * 50)), Color.white, Color.black, 0, null), new Point3D(n.getPitch() / 2d, -0.25, -.45), Angle3D.zeros, new Vector3D(0.45, 0.5, 0.45)));
        parent.addMesh(sm);
        drops.put(n, sm);
    }
    
    private void createSplash(Note n, Material mat) {
        GroupableSimpleMesh sm = new GroupableSimpleMesh(MeshGenerator.sphere(2, mat, new Point3D(n.getPitch() / 2d, 0, -.45), Angle3D.zeros, new Vector3D(0.45, 0.45, 0.45)));
        parent.addMesh(sm);
        splashes.put(n, sm);
    }
    
    @Override
    public Mesh getMesh() {
        return parent;
    }

    @Override
    public void startNote(Note n) {
        createSplash(n, drops.get(n).getVerticies()[0].getMaterial());
        parent.removeMesh(drops.get(n));
        drops.remove(n);
    }

    @Override
    public void updateNote(Note n, int ticks) {
        SimpleMesh sm = splashes.get(n);
        double scale = (double)(n.getDuration() - ticks) / n.getDuration();
        sm.setScale(new Vector3D(0.45 * scale, 0.45 * scale, 0.45 * scale));
    }

    @Override
    public void endNote(Note n) {
        parent.removeMesh(splashes.get(n));
        splashes.remove(n);
    }

    @Override
    public void bufferNote(Note n) {
        createDrop(n);
    }

    @Override
    public void updateBuffer(Note n, int ticks) {
        SimpleMesh sm = drops.get(n);
        sm.setPosition(new Point3D(sm.getPosition().getX(), (getBufferSize() - ticks) * cheight / getBufferSize(), 0));
    }

    @Override
    public int getBufferSize() {
        return 250;
    }
    
}
